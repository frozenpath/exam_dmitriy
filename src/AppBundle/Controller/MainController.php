<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Entity\Place;
use AppBundle\Entity\Review;
use AppBundle\Form\DeleteType;
use AppBundle\Form\PhotoType;
use AppBundle\Form\PlaceType;
use AppBundle\Form\ReviewType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Libs\GetRating;

class MainController extends Controller
{
    /**
     * @Route("/{category_id}", requirements={"category_id": "\d+"})
     * @Method("GET")
     * @param $category_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $category_id = null)
    {
        $place = new Place();
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        if (!$request->get('search')) {
            if ($category_id) {
                $published_places = $this->getDoctrine()->getRepository('AppBundle:Place')
                    ->getPlacesByCategory($category_id);
            } else {
                $published_places = $this->getDoctrine()->getRepository('AppBundle:Place')
                    ->getPublishedPlaces();
            }
        } else {
            $published_places = $this->getDoctrine()->getRepository('AppBundle:Place')
                ->search($request->get('search'));
        }


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $published_places,
            $request->query->getInt('page', 1),
            20
        );

        $rating = new GetRating($this->getDoctrine()->getManager());
        $scores = [];
        $count_photos = [];

        /** @var Place $one_place */
        foreach ($published_places as $one_place) {
            $scores[$one_place->getId()] = $rating->getRatings($one_place->getId());
            $count_photos[$one_place->getId()] = count($one_place->getPhotos());
        }

        $form = $this->createForm(PlaceType::class, $place);

        return $this->render('@App/Main/index.html.twig', array(
            'form' => $form->createView(),
            'categories' => $categories,
            'places' => $pagination,
            'scores' => $scores,
            'count_photos' => $count_photos
        ));
    }

    /**
     * @Route("/place")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $user = $this->getUser();
        $place = new Place();

        $form = $this->createForm(PlaceType::class, $place);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $place->setUser($user);
            $place->setPublished(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();
        }

        return $this->redirectToRoute('app_main_index');
    }

    /**
     * @Route("/place")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function newAction()
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $place = new Place();
        $form = $this->createForm(PlaceType::class, $place);

        return $this->render('@App/Main/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/place/{place_id}")
     * @Method("GET")
     * @param int $place_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function placeAction(int $place_id)
    {
        $user = $this->getUser();
        $review_form = $this->createForm(ReviewType::class);
        $photo_form = $this->createForm(PhotoType::class);
        $delete_form = $this->createForm(DeleteType::class);

        $rating = new GetRating($this->getDoctrine()->getManager());
        $scores = $rating->getRatings($place_id);
        $photos = $this->getDoctrine()->getRepository('AppBundle:Photo')->findBy(['place' => $place_id]);
        $reviews = $this->getDoctrine()->getRepository('AppBundle:Review')->findBy(['place' => $place_id]);
        $place = $this->getDoctrine()->getRepository('AppBundle:Place')->find($place_id);

        if ($user) {
            $user_review = $this->getDoctrine()->getRepository('AppBundle:Review')
                ->getUserReviewsInPlace($user->getId(), $place_id);
        }

        return $this->render('@App/Main/place.html.twig', array(
            'place' => $place,
            'reviews' => $reviews,
            'photos' => $photos,
            'scores' => $scores,
            'user_review' => $user_review ?? null,
            'review_form' => $review_form->createView(),
            'photo_form' => $photo_form->createView(),
            'delete_form' => $delete_form->createView()
        ));
    }

    /**
     * @Route("/review/{place_id}")
     * @Method("POST")
     * @param Request $request
     * @param int $place_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reviewAction(Request $request, int $place_id)
    {
        $user = $this->getUser();
        $place = $this->getDoctrine()->getRepository('AppBundle:Place')->find($place_id);
        $review = new Review();
        $review_form = $this->createForm(ReviewType::class, $review);

        $review_form->handleRequest($request);

        if ($review_form->isSubmitted() && $review_form->isValid()) {
            $review->setUser($user);
            $review->setPlace($place);
            $review->setDatetime(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($review);
            $em->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/review/{place_id}", name="deleteaction")
     * @Method("DELETE")
     * @param Request $request
     * @param int $place_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, int $place_id)
    {
        $user = $this->getUser();
        $user_reviews = $this->getDoctrine()->getRepository('AppBundle:Review')
            ->getUserReviewsInPlace($user->getId(), $place_id);

        $em = $this->getDoctrine()->getManager();

        foreach ($user_reviews as $user_review) {
            $em->remove($user_review);
        }

        $em->flush();

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/photo/{place_id}")
     * @Method("POST")
     * @param Request $request
     * @param int $place_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function photoAction(Request $request, int $place_id)
    {
        $user = $this->getUser();
        $place = $this->getDoctrine()->getRepository('AppBundle:Place')->find($place_id);
        $photo = new Photo();
        $photo_form = $this->createForm(PhotoType::class, $photo);

        $photo_form->handleRequest($request);

        if ($photo_form->isSubmitted() && $photo_form->isValid()) {
            $photo->setUser($user);
            $photo->setPlace($place);

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }


}
