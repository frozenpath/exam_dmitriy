<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('review', TextareaType::class, array(
            'label' => false,
            'required' => false,
        ));
        $builder->add('food_quality', ChoiceType::class, array('choices' => array(
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5'
        ),
            'label' => 'Quality of food',
            'required' => true,
        ));

        $builder->add('service_quality', ChoiceType::class, array('choices' => array(
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5'
        ),
            'label' => 'Service Quality',
            'required' => true,
        ));

        $builder->add('interior', ChoiceType::class, array('choices' => array(
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5'
        ),
            'label' => 'Interior',
            'required' => true,
        ));

        $builder->add('submit', SubmitType::class, array(
            'label' => 'Submit'
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_review_type';
    }
}
