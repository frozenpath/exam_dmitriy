<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'required' => true,
            'label' => 'Title'
        ]);
        $builder->add('category', EntityType::class, array(
            'required' => true,
            'class' => 'AppBundle:Category',
            'choice_label' => 'name',
            'label' => 'Category'
        ));
        $builder->add('description', TextareaType::class, array(
            'required' => true,
            'label' => 'Description'
        ));
        $builder->add('imageFile', VichImageType::class, [
            'required' => true,
            'label' => 'Main photo'
        ]);
        $builder->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_place_type';
    }
}
