<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use AppBundle\Entity\Photo;
use AppBundle\Entity\Place;
use AppBundle\Entity\Review;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadDatas implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user
            ->setEmail('user@user.ru')
            ->setUsername('user')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password1 = $encoder->encodePassword($user, '123');
        $user->setPassword($password1);
        $manager->persist($user);

        $category1 = new Category();
        $category2 = new Category();
        $category3 = new Category();

        $category1->setName('Рестораны');
        $category2->setName('Кафе');
        $category3->setName('Забегаловка');

        $categories = [];

        $manager->persist($category1);
        $manager->persist($category2);
        $manager->persist($category3);

        $categories[] = $category1;
        $categories[] = $category2;
        $categories[] = $category3;

        $places = [];

        for ($i = 0; $i < 30; $i++) {
            $places[] = new Place();
        }

        $j = 0;
        /** @var Place $one_place */
        foreach ($places as $one_place) {
            $one_place->setName('Заведение'.$j);
            $one_place->setPublished(true);
            $k = array_rand($categories);
            $one_place->setCategory($categories[$k]);
            $one_place->setUser($user);
            $one_place->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
            Accusamus accusantium ad commodi consectetur cum distinctio ducimus, esse id illo numquam optio, 
            perferendis provident vitae voluptates voluptatum? Animi dicta minima provident.');
            $one_place->setMainImage('cafe.jpg');
            $manager->persist($one_place);
            $j++;
        }

        $reviews = [];

        for ($i = 0; $i < 30; $i++) {
            $reviews[] = new Review();
        }

        $k = 0;
        /** @var Review $review */
        foreach ($reviews as $review) {
            $review->setUser($user);
            $review->setDatetime(new \DateTime());
            $review->setPlace($places[$k]);
            $review->setReview('Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
            Accusamus accusantium ad commodi consectetur cum distinctio ducimus, esse id illo numquam optio, 
            perferendis provident vitae voluptates voluptatum? Animi dicta minima provident.');
            $review->setFoodQuality(rand(1, 5));
            $review->setInterior(rand(1, 5));
            $review->setServiceQuality(rand(1, 5));
            $manager->persist($review);
            $k++;
        }


        $photos = [];

        for ($i = 0; $i < 30; $i++) {
            $photos[] = new Photo();
        }

        /** @var Photo $photo */
        foreach ($photos as $photo) {
            $photo->setUser($user);
            $k = array_rand($places);
            $photo->setPlace($places[$k]);
            $photo->setPhotoFile('cat3.jpg');
            $manager->persist($photo);
        }

        $manager->flush();
    }
}
