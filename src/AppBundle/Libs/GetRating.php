<?php

namespace AppBundle\Libs;

use Doctrine\ORM\EntityManager;

class GetRating
{
    public $em;

    public function __construct(EntityManager $em = null)
    {
        $this->em = $em;
    }

    public function getRatings(int $place_id)
    {
        $ratings = $this->em->getRepository('AppBundle:Review')->findBy(['place' => $place_id]);
        $scores = [];
        $data = [];

        if (!$ratings) {
            $data['food_score'] = 0;
            $data['service_score'] = 0;
            $data['interior_score'] = 0;
            $data['overall'] = 0;
            $data['count'] = 0;

            return $data;
        }


        foreach ($ratings as $rating) {
            $scores['food'][] = $rating->getFoodQuality();
            $scores['service'][] = $rating->getServiceQuality();
            $scores['interior'][] = $rating->getInterior();
        }

        $count = count($ratings);
        $data['food_score'] = number_format(array_sum($scores['food']) / $count, 1);
        $data['service_score'] = number_format(array_sum($scores['service']) / $count, 1);
        $data['interior_score'] = number_format(array_sum($scores['interior']) / $count, 1);
        $data['overall'] = number_format(array_sum($data) / count($data), 1);
        $data['count'] = $count;

//        if (empty($data)) {
//            $data['food_score'] = 0;
//            $data['service_score'] = 0;
//            $data['interior_score'] = 0;
//            $data['overall'] = 0;
//            $data['count'] = 0;
//        }

        return $data;
    }

    public function getCountPhotos()
    {
        $ratings = $this->em->getRepository('AppBundle:photo')->findBy(['place' => $place_id]);
    }
}
