<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="review", type="text", nullable=true, unique=false)
     */
    private $review;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Place", inversedBy="reviews")
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="reviews")
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(name="food_quality", type="integer", nullable=false)
     */
    private $food_quality;

    /**
     * @var int
     * @ORM\Column(name="service_quality", type="integer", nullable=false)
     */
    private $service_quality;

    /**
     * @var int
     * @ORM\Column(name="interior", type="integer", nullable=false)
     */
    private $interior;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set review
     *
     * @param string $review
     *
     * @return Review
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set place
     *
     * @param \AppBundle\Entity\Place $place
     *
     * @return Review
     */
    public function setPlace(\AppBundle\Entity\Place $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \AppBundle\Entity\Place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Review
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set foodQuality
     *
     * @param integer $foodQuality
     *
     * @return Review
     */
    public function setFoodQuality($foodQuality)
    {
        $this->food_quality = $foodQuality;

        return $this;
    }

    /**
     * Get foodQuality
     *
     * @return integer
     */
    public function getFoodQuality()
    {
        return $this->food_quality;
    }

    /**
     * Set serviceQuality
     *
     * @param integer $serviceQuality
     *
     * @return Review
     */
    public function setServiceQuality($serviceQuality)
    {
        $this->service_quality = $serviceQuality;

        return $this;
    }

    /**
     * Get serviceQuality
     *
     * @return integer
     */
    public function getServiceQuality()
    {
        return $this->service_quality;
    }

    /**
     * Set interior
     *
     * @param integer $interior
     *
     * @return Review
     */
    public function setInterior($interior)
    {
        $this->interior = $interior;

        return $this;
    }

    /**
     * Get interior
     *
     * @return integer
     */
    public function getInterior()
    {
        return $this->interior;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Review
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
}
